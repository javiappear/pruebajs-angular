**Repositorio donde reside la parte del cliente de la pruebaJS**

## Instalación y puesta en marcha

1. Instalar los paquetes
```sh
npm install
```
2. Asegurarse de tener el servidor y la BBDD ejecutándose

3. Ejecutar la aplicación
```sh
npm start
```