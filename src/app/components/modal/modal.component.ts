import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

    @ViewChild('modal') modal: ElementRef;

    public isOpened = false;
    public isClosing = false;

    constructor() {}

    ngOnInit() {}

    open() {
        if (!this.modal) return;

        setTimeout(() => {
            this.isOpened = true;
            this.isClosing = false;
        });
    }

    close() {
        if (!this.modal) return;

        this.isClosing = true;

        setTimeout(() => {
            if (this.isClosing) {
                this.isOpened = false;
                this.isClosing = false;
            }
        }, 200);
    }
}