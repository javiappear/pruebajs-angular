import {Injectable} from '@angular/core';
import {ConfigService} from '@services/config.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable()
export class FeedsService {

    resource = 'feeds';

    baseUrl = this.configService.serverURL + this.resource;

    constructor(private configService: ConfigService,
                private http: HttpClient,
                private router: Router) {
    }

    /**
     * Returns all the feeds in the database
     * @return Promise data returned
     */
    getFeeds() {
        return this.http.get(this.baseUrl).toPromise().then((res: any) => res.data);
    }

    /**
     * Returns a feed given the ID
     * @param String id Identifier of the feed
     * @return Promise data returned
     */
    getFeedById(id: string) {
        return this.http.get(this.baseUrl + '/' + id).toPromise().then((res: any) => res.data);
    }

    /**
     * Returns today's feeds
     * @return Promise data returned
     */
    getTodayFeeds() {
        return this.http.get(this.baseUrl + '/today').toPromise().then((res: any) => res.data);
    }

    /**
     * Creates a feed given an object
     * @param Object data={} Data of the feed
     * @return Promise data returned
     */
    createFeed(data: any = {}) {
        return this.http.post(this.baseUrl, data).toPromise().then((res: any) => res.data);
    }

    /**
     * Updates a feed
     * @param Object data={} Data of the feed
     * @return Promise data returned
     */
    updateFeed(feed: any = {}) {
        return this.http.put(this.baseUrl + '/' + feed.id, feed).toPromise().then((res: any) => res.data);
    }

    /**
     * Returns today's feeds
     * * @param String id Identifier of the feed
     * @return Promise data returned
     */
    deleteFeed(id: string) {
        return this.http.delete(this.baseUrl + '/' + id).toPromise();
    }

    /**
     * Navigates to the feed's main page
     */
    goMainPage() {
        this.router.navigateByUrl(this.resource);
    }

    /**
     * Navigates to a feed details page
     */
    goDetailPage(id) {
        this.router.navigateByUrl(this.resource + '/edit/' + id);
    }
}
