import {Component, OnInit} from '@angular/core';
import {FeedsService} from '@modules/feeds/feeds.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SafeHtml} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToolsService} from '@services/tools.service';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

    feed: any;

    loading = true;

    editing = false;

    safeBody: SafeHtml = '';

    editForm: FormGroup;

    constructor(public feedsService: FeedsService,
                private route: ActivatedRoute,
                private router: Router,
                private formBuilder: FormBuilder,
                private toolsService: ToolsService) {
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');

        // Init the form
        this.editForm = this.formBuilder.group({
            title: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
            body: ['', [Validators.required, Validators.minLength(5)]],
            publisher: ['', Validators.required]
        });

        // Get the data
        this.feedsService.getFeedById(id).then(feed => {
            this.feed = feed;
            this.safeBody = this.toolsService.safeHTML(this.toolsService.nl2br(this.feed.body || ''));
            this.loading = false;
            this.assignFeedToForm();
        }).catch(error => {
            console.error(`No se ha podido recuperar el feed con id ${id}: ${error}`);
            this.feedsService.goMainPage();
        });
    }

    /**
     * Updates the feed
     */
    async updateFeed() {
        if (!this.feed || !this.editForm) {
            return;
        }

        try {
            const objectToUpdate = Object.assign({id: this.feed.id}, this.editForm.value);

            // Update object
            const feedUpdated = await this.feedsService.updateFeed(objectToUpdate);

            console.log(`Se ha actualizado correctamente el feed con id ${this.feed.id}`);

            // Update local values
            this.feed = feedUpdated;
            this.editing = false;
        } catch (error) {
            console.error(`No se ha podido actualizar el feed con id ${this.feed.id}: ${error}`);
        }
    }

    /**
     * Deletes the feed
     */
    deleteFeed() {
        if (!this.feed) {
            return;
        }

        this.feedsService.deleteFeed(this.feed.id).then(() => {
            console.log('Feed eliminado correctamente');
            this.feedsService.goMainPage();
        });
    }

    /**
     * Init the form with feed values
     */
    assignFeedToForm() {
        if (!this.feed) {
            return;
        }

        this.editForm.setValue({
            title: this.feed.title,
            body: this.feed.body,
            publisher: this.feed.publisher
        });
    }
}
