import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FeedsRoutingModule} from './feeds-routing.module';
import {ListComponent} from './list/list.component';
import {FeedsService} from '@modules/feeds/feeds.service';
import { RowComponent } from './row/row.component';
import { CreateComponent } from './create/create.component';
import {ComponentsModule} from '@components/components.module';
import { DetailComponent } from './detail/detail.component';

@NgModule({
    imports: [
        CommonModule,
        FeedsRoutingModule,
        ComponentsModule
    ],
    declarations: [ListComponent, RowComponent, CreateComponent, DetailComponent],
    providers: [FeedsService]
})
export class FeedsModule {
}
