import {Component, OnInit} from '@angular/core';
import {FeedsService} from '@modules/feeds/feeds.service';

@Component({
    selector: 'app-feeds-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    feeds: any[] = [];

    loading = true;

    creating = false;

    constructor(public feedsService: FeedsService) {
    }

    ngOnInit() {
        // Load the data => it's possible to call an async function to load with await the data
        this.feedsService.getTodayFeeds().then(feeds => {
            this.feeds = feeds;
            this.loading = false;
        });
    }

    /**
     * Process the result when creating a feed
     */
    resultCreatingFeed(evt) {
        if (!evt) {
            return;
        }

        this.feeds.push(evt);

        this.creating = false;
    }
}
