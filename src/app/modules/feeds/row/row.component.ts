import {Component, Input, OnInit} from '@angular/core';
import {FeedsService} from '@modules/feeds/feeds.service';

@Component({
    selector: 'app-feed-row',
    templateUrl: './row.component.html',
    styleUrls: ['./row.component.css']
})
export class RowComponent implements OnInit {

    @Input('feed') feed: any;

    constructor(public feedsService: FeedsService) {
    }

    ngOnInit() {}
}
