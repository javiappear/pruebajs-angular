import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FeedsService} from '@modules/feeds/feeds.service';

@Component({
    selector: 'app-feeds-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    addForm: FormGroup;

    @Output() result: EventEmitter<any> = new EventEmitter();

    constructor(private formBuilder: FormBuilder, private feedsService: FeedsService) {
    }

    ngOnInit() {
        // Init the form
        this.addForm = this.formBuilder.group({
            title: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
            body: ['', [Validators.required, Validators.minLength(5)]],
            publisher: ['', Validators.required]
        });
    }

    /**
     * Creates a feed
     */
    async createFeed() {
        if (!this.addForm) {
            return;
        }

        try {
            // Retrieve inserted doc
            const feedInserted = await this.feedsService.createFeed(this.addForm.value);

            // Reset the form values
            this.addForm.reset();

            // Output the result
            this.result.emit(feedInserted);
        } catch (error) {
            console.error(`No se ha podido crear el feed: ${error}`);
        }
    }
}
