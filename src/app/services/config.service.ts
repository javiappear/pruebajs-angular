import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';

@Injectable()
export class ConfigService {
    // Port of this app
    public port = 4200;

    // Port of the server
    public serverPort = 3000;

    // Set url
    public url = environment.production ? 'url-produccion' : 'http://localhost:';

    // Url of this application according to the environment
    public appURL = this.url + this.port + '/';

    // Server url
    public serverURL = this.url + this.serverPort + '/';
}
