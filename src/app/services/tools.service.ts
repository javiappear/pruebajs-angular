import {Injectable} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
})
export class ToolsService {

    constructor(private domSanitizer: DomSanitizer) {
    }

    /**
     * Transforms end lines to <br> tags
     * @param String id='' String to transform
     * @param Boolean is_html=false Determines if the string is xhtml
     * @return String transformed
     */
    nl2br(str, is_xhtml?) {
        const breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';

        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }

    /**
     * Returns safeHTML from string
     * @param String html String to secure
     * @return SafeHtml Secure string
     */
    safeHTML(html: string): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(html);
    }
}
