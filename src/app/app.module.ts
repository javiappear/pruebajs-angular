import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {AppComponent} from './app.component';
import {ComponentsModule} from './components/components.module';
import {ComponentsService} from './components/components.service';
import {AppRoutingModule} from './app-routing.module';
import {ConfigService} from '@services/config.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ComponentsModule,
        HttpClientModule
    ],
    providers: [ComponentsService, ConfigService],
    bootstrap: [AppComponent]
})
export class AppModule {}
